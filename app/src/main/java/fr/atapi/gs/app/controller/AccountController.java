package fr.atapi.gs.app.controller;

import fr.atapi.gs.common.utils.JwtUtil;
import fr.atapi.gs.core.auth.gst.users.UserService;
import fr.atapi.gs.model.security.AuthenticationResponse;
import fr.atapi.gs.model.security.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("${spring.data.rest.base-path}/secure")
class AccountController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody User user) throws AuthenticationException {
        Authentication authentication = null;
        ResponseEntity response = null;

        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            user.getUsername(),
                            user.getPassword()
                    )
            );
        } catch (BadCredentialsException e) {
            log.info("Bad password or username");
        }

        if (authentication != null) {
            SecurityContextHolder.getContext().setAuthentication(authentication);
            User uRetour = this.userService.findByUsername(user.getUsername());
            final String token = jwtTokenUtil.generateToken(authentication);
            response = ResponseEntity.ok(new AuthenticationResponse(token, uRetour));
        }
        return response;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public User saveUser(@RequestBody User user) {
        return userService.save(user);
    }

    @RequestMapping(value = "/get-user-details/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> getUser(@PathVariable String id) {
        ResponseEntity response = null;
        User user;
        if (id != null) {
            user = userService.findUserById(Integer.valueOf(id));
            response = ResponseEntity.ok(user);
        }
        return response;
    }

    @RequestMapping(value = "/update-user", method = RequestMethod.POST)
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        ResponseEntity response = null;
        User userToUpdate;
        if (user != null && user.getId() != null) {
            userToUpdate = this.userService.updateUser(user);
            response = ResponseEntity.ok(userToUpdate);
        } else {
            response = ResponseEntity.ok(user);
        }
        return response;

    }

    @PreAuthorize("hasRole('ROLE_Administrator')")
    @RequestMapping(value = "/adminping", method = RequestMethod.GET)
    public String adminPing() {
        return "Only Admins Can Read This";
    }

    @PreAuthorize("hasRole('ROLE_Administrator')")
    @RequestMapping(value = "/userping", method = RequestMethod.GET)
    public String userPing() {
        return "Any User Can Read This";
    }

}
