package fr.atapi.gs.app.controller;

import fr.atapi.gs.model.gs.Category;
import fr.atapi.gs.model.gs.Product;
import fr.atapi.gs.repository.gs.CategoryRepository;
import fr.atapi.gs.repository.gs.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * * this class is implemented in 13/02/2022 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("${spring.data.rest.base-path}/gs")
@Slf4j
public class GSConstroller {


    @Autowired
    ProductRepository repoProduct;

    @Autowired
    CategoryRepository repoCategory;

    @RequestMapping(value = "/add-product", method = RequestMethod.POST)
    public ResponseEntity<Product> addNewProduct(@RequestBody Product product) {

        this.repoProduct.save(product);
        if(product.isPersisted()){
            log.info("a new product has been inserted {}", product );
        }

        ResponseEntity<Product> response = ResponseEntity.ok(null);
        if(product != null){
            response = ResponseEntity.ok(product);
        }
        return response;
    }

    @RequestMapping(value = "/get-all-products", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getAllProducts() {

        List<Product> result = this.repoProduct.findAll();

        ResponseEntity<List<Product>> response = ResponseEntity.ok(null);
        if(!result.isEmpty()){
            response = ResponseEntity.ok(result);
        }
        return response;
    }

    @RequestMapping(value = "/get-all-availableProducts", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getAllQteDisponible(String idCategory) {

        List<Product> result = this.repoProduct.findAvailableProductsByCategory(idCategory);

        ResponseEntity<List<Product>> response = ResponseEntity.ok(null);
        if(!result.isEmpty()){
            response = ResponseEntity.ok(result);
        }
        return response;
    }

    @RequestMapping(value = "/add-category", method = RequestMethod.POST)
    public ResponseEntity<Category> addNewCategory(@RequestBody Category category) {

        this.repoCategory.save(category);
        if(category.isPersisted()){
            log.info("a new category has been inserted {}", category );
        }

        ResponseEntity<Category> response = ResponseEntity.ok(null);
        if(category != null){
            response = ResponseEntity.ok(category);
        }
        return response;
    }

    @RequestMapping(value = "/get-all-categories", method = RequestMethod.GET)
    public ResponseEntity<List<Category>> getAllCategories() {

        List<Category> result = this.repoCategory.findAll();

        ResponseEntity<List<Category>> response = ResponseEntity.ok(null);
        if(!result.isEmpty()){
            response = ResponseEntity.ok(result);
        }
        return response;
    }

}
