package fr.atapi.gs.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"fr.atapi.gs.app", "fr.atapi.gs.repository", "fr.atapi.gs.model", "fr.atapi.gs.dal", "fr.atapi.gs.common", "fr.atapi.gs.core"})
@EntityScan("fr.atapi.gs.model")
@EnableCaching
@EnableJpaRepositories("fr.atapi.gs.repository")
public class AppStarter {



    public static void main(String[] args) {
        SpringApplication.run(AppStarter.class, args);


    }

}
