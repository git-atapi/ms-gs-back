package fr.atapi.gs.model.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
@AllArgsConstructor

public class AuthenticationResponse implements Serializable {

    private final String token;
    private User user;

    public AuthenticationResponse(String jwt) {
        this.token = jwt;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "AuthenticationResponse{" +
                "token='" + token + '\'' +
                ", user=" + user +
                '}';
    }
}