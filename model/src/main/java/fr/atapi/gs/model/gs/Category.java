package fr.atapi.gs.model.gs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

/**
 * * this class is implemented in 13/02/2022 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("Category")
public class Category {

    @Id
    private String id;

    @Indexed
    private String nomCategory;

    boolean persisted = false;


}
