package fr.atapi.gs.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Khaled AMIRAT on 14/03/2021
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity
public class Version {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String author;

    @Override
    public String toString() {
        return "Version{" +
                "id=" + id +
                ", author='" + author + '\'' +
                '}';
    }
}
