package fr.atapi.gs.model.gs;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * * this class is implemented in 13/02/2022 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("Product")
public class Product implements Serializable {

    @Id
    private String id;

    @Indexed
    private String nomProduit;
    private int qte;

    private long prix;

    private String idCategory;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    LocalDate datePeremption;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    LocalDate dateArrivee;

    boolean persisted = false;

    /**
     * j'aurai du faire une autre entité pour ça
     * commande, un stock, marché... et tout ...,
     * mais je vais saisir la qte consommée manuellement lors de l'insertion.
     */
    private int qteConsom;

    public Product(String nomProduit, int qte, long prix, String idCategory, LocalDate datePeremption, LocalDate dateArrivee, boolean persisted, int qteConsom) {
        this.nomProduit = nomProduit;
        this.qte = qte;
        this.prix = prix;
        this.idCategory = idCategory;
        this.datePeremption = datePeremption;
        this.dateArrivee = dateArrivee;
        this.persisted = persisted;
        this.qteConsom = qteConsom;
    }
}