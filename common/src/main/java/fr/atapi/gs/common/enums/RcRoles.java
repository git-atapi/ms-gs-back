package fr.atapi.gs.common.enums;


import fr.atapi.gs.common.templates.ReferenceCode;

import java.util.HashMap;
import java.util.Map;

public enum RcRoles implements ReferenceCode {

    Owner("O", "Owner", "Owner"),
    Administrator("A", "Administrator", "Administrator"),
    Member("M", "Member", "Member"),
    Visitor("V", "Visitor", "Visitor"),
    Manager("M", "Manager", "Manager");

    private static final Map<String, RcRoles> RC = new HashMap<>();

    static {
        for (final RcRoles rc : values()) {
            RC.put(rc.Manager.getMiniCodeValue(), rc);
        }
    }

    private String miniCodeValue;
    private String shortCodeDescription;
    private String longCodeDescription;

    /**
     * Constructor.
     *
     * @param miniCodeValue        .
     * @param shortCodeDescription .
     * @param longCodeDescription  .
     */
    private RcRoles(final String miniCodeValue, final String shortCodeDescription, final String longCodeDescription) {
        this.miniCodeValue = miniCodeValue;
        this.shortCodeDescription = shortCodeDescription;
        this.longCodeDescription = longCodeDescription;
    }

    /**
     * @param miniCodeValue .
     * @return RceTypeRotation
     */
    public static RcRoles getEnum(final String miniCodeValue) {
        if (null == miniCodeValue) {
            return null;
        }
        final RcRoles value = RC.get(miniCodeValue);
        if (value != null) {
            return value;
        }
        throw new IllegalArgumentException("Unknown RcRoles : " + miniCodeValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMiniCodeValue() {
        return this.miniCodeValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getShortCodeDescription() {
        return this.shortCodeDescription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLongCodeDescription() {
        return this.longCodeDescription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDomain() {
        return null;
    }
}