package fr.atapi.gs.common.templates;

import java.io.Serializable;

/**
 * * this class is implemented in 05/11/2021 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/
public interface ReferenceCode extends Serializable {

    /**
     * Return the value of miniCodeValue.
     *
     * @return value of miniCodeValue.
     */
    String getMiniCodeValue();

    /**
     * Return the value of shortCodeDescription.
     *
     * @return value of shortCodeDescription.
     */
    String getShortCodeDescription();

    /**
     * Return the value of longCodeDescription.
     *
     * @return the value of longCodeDescription.
     */
    String getLongCodeDescription();

    /**
     * Return the domain value
     *
     * @return the of domain
     */
    String getDomain();

}
