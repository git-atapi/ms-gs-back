package fr.atapi.gs.core.mapper;

import fr.atapi.gs.model.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * * this class is implemented in 20/11/2021 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/

@Component
public class UserMapper {

    @Autowired
    @Qualifier("userMapperBean")
    private BCryptPasswordEncoder bcryptEncoder;

    public User mapUpdateUser(User userToUpdate, User userPayloadWithUpdates) {
        if (userToUpdate != null && userPayloadWithUpdates != null) {
            if (userPayloadWithUpdates.getFirstName() != null)
                userToUpdate.setFirstName(userPayloadWithUpdates.getFirstName());
            if (userPayloadWithUpdates.getUsername() != null)
                userToUpdate.setLastName(userPayloadWithUpdates.getLastName());
            if (userPayloadWithUpdates.getEmail() != null) userToUpdate.setEmail(userPayloadWithUpdates.getEmail());
            if (userPayloadWithUpdates.getAddress() != null)
                userToUpdate.setAddress(userPayloadWithUpdates.getAddress());
            if (userPayloadWithUpdates.getPhone() != null) userToUpdate.setPhone(userPayloadWithUpdates.getPhone());

        }

        return userToUpdate;
    }


    @Bean("userMapperBean")
    public static BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
