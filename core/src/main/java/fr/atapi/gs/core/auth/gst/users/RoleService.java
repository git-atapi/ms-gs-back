package fr.atapi.gs.core.auth.gst.users;

import fr.atapi.gs.model.security.Role;
import fr.atapi.gs.repository.security.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * * this class is implemented in 04/11/2021 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/

@Service
public class RoleService {

    @Autowired
    private RoleRepo roleRepo;

    public Role findByName(String name) {
        Role role = roleRepo.findRoleByName(name);
        return role;
    }

    public Role save(Role role) {
        return this.roleRepo.save(role);
    }
}
