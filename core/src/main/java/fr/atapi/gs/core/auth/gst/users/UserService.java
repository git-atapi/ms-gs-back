package fr.atapi.gs.core.auth.gst.users;

import fr.atapi.gs.common.enums.RcRoles;
import fr.atapi.gs.core.mapper.UserMapper;
import fr.atapi.gs.model.security.Role;
import fr.atapi.gs.model.security.User;
import fr.atapi.gs.repository.security.UserRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Transient;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * * this class is implemented in 04/11/2021 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/

@Service
@Transient
@Transactional
@RequiredArgsConstructor
@Slf4j
public class UserService implements UserDetailsService {


    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    @Qualifier("userServiceBean")
    private BCryptPasswordEncoder bcryptEncoder;

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);
        if (user == null) {
            log.info("error on finding user");
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        });
        return authorities;
    }

    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        userRepo.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    public User findUserById(Integer id) {
        User user = null;
        if (id != null) {
            user = this.userRepo.findById(id).get();
        }
        return user;
    }

    public User save(User user) {

        user.setPassword(bcryptEncoder.encode(user.getPassword()));

        Role role = roleService.findByName(RcRoles.Member.getShortCodeDescription());
        if (role == null) {
            role = new Role();
            role.setName(RcRoles.Member.getShortCodeDescription());
            role.setDescription(RcRoles.Member.getLongCodeDescription());
            roleService.save(role);
        }
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(role);

        if (user.getEmail() != null && user.getEmail().contains("@") && user.getEmail().split("@")[1].equals("atapi.fr")) {
            role = roleService.findByName(RcRoles.Administrator.getShortCodeDescription());
            if (role == null) {
                role = new Role();
                role.setName(RcRoles.Administrator.getShortCodeDescription());
                role.setDescription(RcRoles.Administrator.getLongCodeDescription());
                roleService.save(role);
            }
            roleSet.add(role);
        }

        user.setRoles(roleSet);
        return userRepo.save(user);
    }

    public User updateUser(User user) {
        User userToUpdate = null;
        if (user != null && user.getUsername() != null) {
            userToUpdate = this.findByUsername(user.getUsername());
            userToUpdate = this.userMapper.mapUpdateUser(userToUpdate, user);
            this.userRepo.save(userToUpdate);
        }
        return userToUpdate;
    }

    @Bean("userServiceBean")
    public static BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
