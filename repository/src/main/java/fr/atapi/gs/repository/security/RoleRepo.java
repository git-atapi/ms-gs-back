package fr.atapi.gs.repository.security;

import fr.atapi.gs.model.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * * this class is implemented in 04/11/2021 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/

@Repository
public interface RoleRepo extends JpaRepository<Role, Integer> {

    Role findRoleByName(String name);

}
