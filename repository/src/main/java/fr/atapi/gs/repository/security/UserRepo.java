package fr.atapi.gs.repository.security;

import fr.atapi.gs.model.security.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * * this class is implemented in 04/11/2021 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/

@Repository
public interface UserRepo extends JpaRepository<User, Integer> {
    User findByUsername(String username);
}
