package fr.atapi.gs.repository.gs;

import fr.atapi.gs.model.gs.Category;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * * this class is implemented in 13/02/2022 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/

@Slf4j
@Repository
public class CategoryRepository {

    public static final String HASH_KEY = "Category";
    @Autowired
    private RedisTemplate template;

    public Category save(Category category){
        if (category.getId() == null) {
            category.setId(UUID.randomUUID().toString());
        }

        if(insertionAllowed(category)) {
            category.setPersisted(true);
            template.opsForHash().put(HASH_KEY, "" + category.getId(), category);
            log.info("category already exist {}", category);
        }

        return category;
    }

    public List<Category> findAll(){
        return (List<Category>)template.opsForHash().values(HASH_KEY);
    }

    public Category findCategoryById(String id){
        return (Category) template.opsForHash().get(HASH_KEY,id);
    }


    public String deleteCategory(String id){
        template.opsForHash().delete(HASH_KEY,id);
        return "category removed !!";
    }

    public boolean insertionAllowed(Category category){
        boolean exist = false;
        List<Category> list = findAll().stream().filter(c -> c.getNomCategory().equals(category.getNomCategory())).collect(Collectors.toList());
        if(list.isEmpty()){
            exist = true;
            log.info("category already exist {}", category );
        }
        return exist;

    }

}
