package fr.atapi.gs.repository.gs;

import fr.atapi.gs.model.gs.Category;
import fr.atapi.gs.model.gs.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * * this class is implemented in 13/02/2022 by khaled AMIRAT
 * * all copyrights reserved Atapi
 **/

@Slf4j
@Repository
public class ProductRepository {

    public static final String HASH_KEY = "Product";

    @Autowired
    private RedisTemplate template;

    @Autowired
    private CategoryRepository categoryRepository;

    public Product save(Product product){
        if (product.getId() == null) {
            product.setId(UUID.randomUUID().toString());
        }

        if(insertionAllowed(product)) {
            product.setPersisted(true);
            template.opsForHash().put(HASH_KEY, product.getId(), product);
            log.info("product inserted", product);
        } else {
            log.info("product already exist or category not found", product);
        }
        return product;
    }

    public List<Product> findAll(){
        return template.opsForHash().values(HASH_KEY);
    }

    public Product findProductById(String id){
        return (Product) template.opsForHash().get(HASH_KEY,id);
    }


    public String deleteProduct(String id){
         template.opsForHash().delete(HASH_KEY,id);
        return "product removed !!";
    }

    public boolean insertionAllowed(Product product){
        boolean exist = false;
        if(product.getIdCategory() != null){
            exist = this.categoryRepository.findCategoryById(product.getIdCategory()) != null;
        }
        return exist;

    }

    /**
     * 1 / permettre de filter les produits par une catégorie donnée
     * 2 / permettre de grouper les produits de 1 avec leurs nom
     * 3 / permettre de retourner des prod par nom et qté disponible en stock
     *
     * @param catID
     * @return
     */
    public List<Product> findAvailableProductsByCategory(String catID) {
        List<Product> products = this.findAll().stream().filter(product -> product.getIdCategory().equals(catID)).collect(Collectors.toList());


        products = products.stream()
                .collect(Collectors.groupingBy(
                        Product::getNomProduit))
                .values()
                .stream()
                .map(values -> values.stream()
                        .reduce((myObject1, myObject2) -> {
                            var sumQte = myObject1.getQte() + (myObject2.getQte());
                            var sumQteCons = myObject1.getQteConsom() + (myObject2.getQteConsom());
                            var somRest = sumQte - sumQteCons;
                            return  new Product(myObject1.getNomProduit()
                                    ,somRest, myObject1.getPrix(), myObject1.getIdCategory()
                                    ,myObject1.getDatePeremption(), myObject1.getDateArrivee()
                                    ,myObject1.isPersisted()
                                    ,sumQteCons) ;
                        })
                        .get())
                .filter(product -> product.getQte() > 0)
                .collect(Collectors.toList());

        return products;
    }

}